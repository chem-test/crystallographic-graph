from __future__ import annotations

import torch
import torch.nn.functional as F
from torch_geometric.data import Data

class CrystalData(Data):
    def __init__(self, *args, **kwargs):
        
        super(CrystalData, self).__init__(*args, **kwargs)

    @property
    def cell(self) -> torch.FloatTensor:
        return super(CrystalData, self).cell

    def set_cell(self, cell):
        self.cell = cell

    @property
    def pos(self) -> torch.FloatTensor:
        return super(CrystalData, self).pos

    def set_pos(self, pos: torch.FloatTensor):
        self.pos = pos % 1

    @property
    def device(self) -> torch.device:
        return self.cell.device

    @property
    def cell_lengths(self) -> torch.FloatTensor:
        return self.cell.norm(dim=2).t()

    @property
    def cell_angles(self) -> torch.FloatTensor:
        angles = torch.zeros_like(self.cell_lengths)

        i = torch.tensor([0, 1, 2], dtype=torch.long, device=self.device)
        j = torch.tensor([1, 2, 0], dtype=torch.long, device=self.device)
        k = torch.tensor([2, 0, 1], dtype=torch.long, device=self.device)

        cross = torch.cross(self.cell[:, j], self.cell[:, k], dim=2)
        dot = (self.cell[:, j] * self.cell[:, k]).sum(dim=2)

        angles[i, :] = torch.rad2deg(torch.atan2(cross.norm(dim=2), dot).t())

        inv_mask = (cross * self.cell[:, i]).sum(dim=2) < 0
        angles[inv_mask.t()] *= -1

        return angles
